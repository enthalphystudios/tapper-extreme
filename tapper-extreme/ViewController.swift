//
//  ViewController.swift
//  tapper-extreme
//
//  Created by Marco Rodrigues on 26/01/2016.
//  Copyright © 2016 Marco Rodrigues. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // members
    var maxTaps: Int = 0
    var currentTaps: Int = 0
    
    // outlets
    
    @IBOutlet weak var bgLogoImg: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var txtHowManyTaps: UITextField!
    
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var lblTaps: UILabel!


    @IBAction func onBtnPlayPressed(sender: UIButton!) {
        if (txtHowManyTaps.text != nil && txtHowManyTaps.text != "") {
            maxTaps = Int(txtHowManyTaps.text!)!
            currentTaps = 0
            
            showMainView(false)
            updateLblTaps()
        }
    }
    
    @IBAction func onCoinTapped(sender: UIButton!) {
        currentTaps++
        updateLblTaps()
        
        if (isGameOver()) {
            restartGame()
        }
        
    }
    
    func updateLblTaps() {
        lblTaps.text = "\(currentTaps) Taps"
    }
    
    func showMainView(view: Bool) {
        bgLogoImg.hidden = !view
        btnPlay.hidden = !view
        txtHowManyTaps.hidden = !view
        
        btnTap.hidden = view
        lblTaps.hidden = view
    }
    
    func restartGame() {
        txtHowManyTaps.text = ""
        currentTaps = 0
        maxTaps = 0
        showMainView(true)
    }
    
    func isGameOver() -> Bool {
        return currentTaps >= maxTaps
    }
    

}

